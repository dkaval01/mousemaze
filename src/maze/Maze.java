package maze;


/**
 * @author Domantas Kavaliauskas
 *         created on 25-Sep-16
 */

/** A maze class to create a maze in text format
 * where '.' is valid path, 'X' a wall and
 * 'E' an exit.
 */
public class Maze {
    private int rows;
    private int columns;
    private int maxDepth;
    private char[][] cells;
    private boolean[][] visited;

    /**
     * A Maze constructor where the size of maze is expected
     * and each row is defined as a String in array.
     * The loop inside constructor converts strings to char
     * and fills cells array.
     * @param rows defines the height of the maze
     * @param columns defines the width of the maze
     * @param lines an array of strings which represents maze.
     */
    public Maze(int rows, int columns, String[] lines){
        this.rows = rows;
        this.columns = columns;
        this.cells = new char[rows][columns];
        this.visited = new boolean[rows][columns];
        this.maxDepth = rows * columns;
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < columns; j++){
                this.cells[i][j] = lines[i].charAt(j);
                this.visited[i][j] = false;
            }
        }

    }

    /**
     * Returns shortest path of relevant information by giving starting coordinates
     * @param i a row number in a Maze
     * @param j a column number in a Maze
     * @return the distance to  an exit or an answer that such no valid path exists.
     */
    public String calcShortestPath(int i, int j){
        int sPath = shortestPath(i, j);
        if(sPath == -1){
            return "There is no path to exit.";
        }
        if(sPath == -2){
            return "That square is a wall.";
        }
        return "The shortest distance to an exit is " + sPath + ".";
    }

    /**
     * Calculates shortest path and returns numerical form of result
     * @param i a row number in a Maze
     * @param j a column number in a Maze
     * @return distance to shortest path, 0 if start and exit coordinate matches, -2 if start coordinate is a wall,
     * -1 if no path exists
     */
    private int shortestPath(int i, int j) {
        if(cells[i][j] == 'E') return 0;
        if(cells[i][j] == 'X') return -2;
        return iterativeDeepeningDFS(i, j);
    }

    /**
     * Depth limited search method as a part of Iterative Deepening DFS
     * @param i a row coordinate of current cell
     * @param j a column coordinate of current cell
     * @param depth a limit of depth search can reach
     * @return returns true if an exit was found
     */
    private boolean depthLimitedSearch(int i, int j, int depth){
        boolean left = false;
        boolean right = false;
        boolean bottom = false;
        boolean top = false;

        if(cells[i][j] == 'E'){
            return true;
        }

        if(depth>0){
            visited[i][j] = true;

            if( i + 1 < rows && cells[i + 1][j] != 'X' && !visited[i+1][j]) {
                bottom = depthLimitedSearch(i + 1, j, depth-1);
            }
            if(i - 1 > -1 && cells[i - 1][j] != 'X' && !visited[i-1][j]) {
                top = depthLimitedSearch(i - 1, j, depth-1);
            }
            if(j + 1 < columns && cells[i][j + 1] != 'X' && !visited[i][j+1]) {
                right = depthLimitedSearch(i, j + 1, depth-1);
            }
            if(j - 1 > -1 && cells[i][j - 1] != 'X' && !visited[i][j-1]) {
                left = depthLimitedSearch(i, j - 1, depth-1);
            }
            visited[i][j] = false;
            return left||right||bottom||top;
        } else {
            return false;
        }
    }

    /**
     * A method which gradually increases a depth limit for DFS and returns the depth once result is found
     * @param i a row number of starting cell coordinate
     * @param j a column number of starting cell coordinate
     * @return the distance to the result or -1 if no path exists.
     */
    private int iterativeDeepeningDFS(int i, int j){
        int thisDepth = 1;
        boolean foundMin = false;
        while(thisDepth < maxDepth){
            foundMin = depthLimitedSearch(i, j, thisDepth);
            if(foundMin){
                return thisDepth;
            } else {
                thisDepth++;
            }
        }

        return -1;
    }


    public char[][] getCells() {
        return cells;
    }

    public void setCells(char[][] cells) {
        this.cells = cells;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public int getMaxDepth() {
        return maxDepth;
    }

    public void setMaxDepth(int maxDepth) {
        this.maxDepth = maxDepth;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public boolean[][] getVisited() {
        return visited;
    }

    public void setVisited(boolean[][] visited) {
        this.visited = visited;
    }
}
