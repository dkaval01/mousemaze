package maze;

import java.util.Scanner;

/**
 * @author Domantas Kavaliauskas
 *         created on 25-Sep-16
 */
public class MazeSolution {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int rows = in.nextInt();
        int columns = in.nextInt();
        String[] mazeLines = new String[rows];

        for(int i = 0; i < rows; i++){
            mazeLines[i] = in.next();
        }

        Maze maze = new Maze(rows, columns, mazeLines);

        int testCases = in.nextInt();

        for(int j = 0; j < testCases; j++){
            System.out.println(maze.calcShortestPath(in.nextInt(), in.nextInt()));
        }
    }
}
