package maze_tests;

import maze.Maze;
import org.junit.Test;


/**
 * @author Domantas Kavaliauskas
 *         created on 25-Sep-16
 */
public class MazeTest {

    @Test
    public void testCalcShortestPath() throws Exception {
        String[] grid = {
                "......X...",
                "..X...X...",
                "..X...X...",
                "..X.XXXX..",
                "..X....X..",
                "..X....X..",
                "..XXXX....",
                ".....X....",
                ".....X..E.",
                "X....X...."
        };
        Maze first = new Maze(10, 10, grid);
        String expected = "The shortest distance to an exit is 27.";
        assert(expected.equals(first.calcShortestPath(9, 4)));
        String expected2 = "That square is a wall.";
        assert(expected2.equals(first.calcShortestPath(0, 6)));
    }

}
